#include "database.h"
#include "home.h"

Database::Database(QObject *parent) : QObject(parent)
{

}

bool Database::conOpen()
{
    //SQLite database connection
    //
    //----Get the database file from the project folder(in resource) and replace in your computer.
    //----Replace the database file path in the computer in 'setDatabaseName' field within "".
    //----Replace all "\" to "/".
    //----No need to install any database drivers for SQLite databse just replace the file path.
    //----If any issue follow DATABASE INSTRUNTION.txt
    //


    QDir dir("Resource");
    QString Path = dir.absoluteFilePath("address.sqlite");

    addressdb = QSqlDatabase::addDatabase("QSQLITE");
    addressdb.setDatabaseName(Path);

    if(!addressdb.open())
    {
        qDebug()<<"Database connection faild...!";
        return false;
    }
    else
    {
        qInfo()<<"Database Connection Successful..!";
        return true;
    }
}

void Database::conClose()
{
    addressdb.close();
    addressdb.removeDatabase(QSqlDatabase::defaultConnection);
}

bool Database::userAuthantication(QString username, QString password)
{
    conOpen();
    QSqlQuery quary;
    QString decodePassword = QString(QCryptographicHash::hash((password.toUtf8()),QCryptographicHash::Md5).toHex());
    quary.prepare("SELECT * FROM tbladmin WHERE username='"+username+"' and password='"+decodePassword+"'");
    if(quary.exec())
    {
        int quaryCount=0;
        while(quary.next())
        {
            quaryCount++;
        }

        if(quaryCount==1)
        {
            conClose();
            return true;
        }
        else
        {
            conClose();
            return false;
        }
    }
    else
    {
       conClose();
       return false;
    }
}

User *Database::getUser(QString name, QString password)
{
    conOpen();
    QSqlQuery quary;
    QString decodePassword = QString(QCryptographicHash::hash((password.toUtf8()),QCryptographicHash::Md5).toHex());
    quary.prepare("SELECT * FROM tbladmin WHERE username='"+name+"' and password='"+decodePassword+"'");
    User *u = new User();
    if(quary.exec())
    {
    while(quary.next())
    {
        qDebug()<<quary.value("adminid").toString();
        u->setAdminid(quary.value("adminid").toString());
        u->setUsername(quary.value("username").toString());
        break;
    }
    conClose();
    }
    return u;

}

bool Database::saveRecord(QString id, QString firstName, QString lastName, QString email, QString tel, QString address, QString adminid)
{
    conOpen();
    QSqlQuery quary;
    quary.prepare("INSERT INTO tblinfo (firstName,lastName,email,tel,address,adminid) VALUES ('"+firstName+"','"+lastName+"','"+email+"','"+tel+"','"+address+"','"+adminid+"')");
    if(quary.exec())
    {
        return true;
        conClose();
    }
    else
    {
        return false;
        conClose();
    }
}

bool Database::updateRecord(QString id, QString firstName, QString lastName, QString email, QString tel, QString address)
{
    conOpen();
    QSqlQuery quary;
    quary.prepare("UPDATE tblinfo set firstName='"+firstName+"',lastName='"+lastName+"',email='"+email+"',tel='"+tel+"',address='"+address+"' WHERE id='"+id+"'");
    if(quary.exec())
    {
        return true;
        conClose();
    }
    else
    {
        return false;
        conClose();
    }
}

bool Database::deleteRecord(QString id)
{
    conOpen();
    QSqlQuery quary;
    quary.prepare("DELETE FROM tblinfo WHERE id='"+id+"'");
    if(quary.exec())
    {
        return true;
        conClose();
    }
    else
    {
        return false;
        conClose();
    }
}

