#ifndef RECORD_H
#define RECORD_H

#include <QObject>

class Record : public QObject
{
    Q_OBJECT
public:
    explicit Record(QObject *parent = 0);

    int getId() const;
    void setId(int value);

    QString getFirstName() const;
    void setFirstName(const QString &value);

    QString getLastname() const;
    void setLastname(const QString &value);

    QString getEmail() const;
    void setEmail(const QString &value);

    QString getTel() const;
    void setTel(const QString &value);

    QString getAddress() const;
    void setAddress(const QString &value);

    int getAdminId() const;
    void setAdminId(int value);

private:
    int id;
    QString firstName;
    QString lastname;
    QString email;
    QString tel;
    QString address;
    int adminId;

signals:

public slots:
};

#endif // RECORD_H
