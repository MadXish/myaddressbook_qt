#include "record.h"

Record::Record(QObject *parent) : QObject(parent)
{

}

int Record::getId() const
{
    return id;
}

void Record::setId(int value)
{
    id = value;
}

QString Record::getFirstName() const
{
    return firstName;
}

void Record::setFirstName(const QString &value)
{
    firstName = value;
}

QString Record::getLastname() const
{
    return lastname;
}

void Record::setLastname(const QString &value)
{
    lastname = value;
}

QString Record::getEmail() const
{
    return email;
}

void Record::setEmail(const QString &value)
{
    email = value;
}

QString Record::getTel() const
{
    return tel;
}

void Record::setTel(const QString &value)
{
    tel = value;
}

QString Record::getAddress() const
{
    return address;
}

void Record::setAddress(const QString &value)
{
    address = value;
}

int Record::getAdminId() const
{
    return adminId;
}

void Record::setAdminId(int value)
{
    adminId = value;
}

