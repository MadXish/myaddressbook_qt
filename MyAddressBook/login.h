#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include "home.h"
#include "register.h"
#include "database.h"
#include "user.h"

namespace Ui {
class Login;
}

class Login : public QMainWindow
{
    Q_OBJECT


public:
    explicit Login(QWidget *parent = 0);
    static QString loginID;
    Database db;
    ~Login();

private slots:
    void on_btnLogin_clicked();

    void on_btnSignup_clicked();

    void on_actionClose_2_triggered();

    void on_actionClose_triggered();

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
