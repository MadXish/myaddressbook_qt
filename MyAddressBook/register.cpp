#include "register.h"
#include "ui_register.h"
#include <QMessageBox>

Register::Register(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Register)
{
    ui->setupUi(this);
}

Register::~Register()
{
    delete ui;
}


void Register::on_btnCancle_clicked()
{
    //Login mainwindow;
    //mainwindow.show();
    this->close();
}

void Register::on_addUser_clicked()
{
    Database con;

    QString username,password,passwordagain;

    username=ui->regUsername->text();
    password=ui->regPassword->text();
    passwordagain=ui->regPasswordagain->text();

    if(!con.conOpen())
    {
        qDebug()<<"Database Connection Faild...!";
        return;
    }

    if(password == passwordagain)
    {
    QString encodePassword = QString(QCryptographicHash::hash((password.toUtf8()),QCryptographicHash::Md5).toHex());
    con.conOpen();
    QSqlQuery quary;
    quary.prepare("INSERT INTO tbladmin (username,password) VALUES ('"+username+"','"+encodePassword+"')");

    if(quary.exec())
    {
        QMessageBox::information(this,tr("Add User"),tr("User Added Succesfully...!"));
        con.conClose();
    }
    else
    {
       QMessageBox::critical(this,tr("Error Message"),quary.lastError().text());
    }

    }
    else
    {
        QMessageBox::warning(this,tr("Registrtion Error"),tr("Password fields Are Not Match,Plase Check Again..!"));
    }
}
