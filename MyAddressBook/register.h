#ifndef REGISTER_H
#define REGISTER_H

#include <QDialog>
#include "login.h"
#include "database.h"

namespace Ui {
class Register;
}

class Register : public QDialog
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = 0);
    ~Register();

private slots:

    void on_btnCancle_clicked();

    void on_addUser_clicked();

private:
    Ui::Register *ui;
};

#endif // REGISTER_H
