#ifndef USER_H
#define USER_H

#include <QObject>

class User : public QObject
{
    Q_OBJECT
public:
    explicit User(QObject *parent = 0);

    QString getUsername() const;
    void setUsername(const QString &value);

    QString getPassword() const;
    void setPassword(const QString &value);

    QString getAdminid() const;
    void setAdminid(const QString &value);

    QString adminid;
    QString username;
    QString password;

private:

signals:

public slots:
};

#endif // USER_H
