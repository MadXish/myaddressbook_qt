#include "user.h"

User::User(QObject *parent) : QObject(parent)
{

}

QString User::getUsername() const
{
    return username;
}

void User::setUsername(const QString &value)
{
    username = value;
}

QString User::getPassword() const
{
    return password;
}

void User::setPassword(const QString &value)
{
    password = value;
}

QString User::getAdminid() const
{
    return adminid;
}

void User::setAdminid(const QString &value)
{
    adminid = value;
}

