#include "home.h"
#include "ui_home.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

Home::Home(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Home)
{
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowFlags(this->windowFlags() & Qt::WindowMinimizeButtonHint);

    ui->setupUi(this);
    Database con;
    if(!con.conOpen())
        ui->label->setText("Database Connection faild...!");
    else
        ui->label->setText("Records read successful..!");

    ui->txthiddenid->setDisabled(true);
    ui->txthiddenid->setVisible(false);
    ui->txtID->setVisible(false);
    ui->label_7->setVisible(false);
    ui->btnLoad->setVisible(false);


    Login loginID;
    ui->txthiddenid->setText(loginID.loginID);

    //Load data to TableView
    QSqlQueryModel *model=new QSqlQueryModel();

    con.conOpen();

    //To TableView
    QSqlQuery* quary=new QSqlQuery(con.addressdb);
    quary->prepare("SELECT firstName AS 'First Name',lastName AS 'Last Name',email AS Email,tel AS 'Telephone No',address AS Address FROM tblinfo WHERE adminid='"+ui->txthiddenid->text()+"'");
    quary->exec();
    model->setQuery(*quary);
    ui->tblInfo->setModel(model);

    con.conClose();
    //qDebug()<<(model->rowCount());
}

Home::~Home()
{
    delete ui;
}

void Home::on_btnSave_clicked()
{
    //Save information to database
    Login loginID;
    ui->txthiddenid->setText(loginID.loginID);

    Database con;
    QString id,firstName,lastName,email,tel,address,adminid;

    id=ui->txtID->text();
    firstName=ui->txtFname->text();
    lastName=ui->txtLname->text();
    email=ui->txtEmail->text();
    tel=ui->txtTel->text();
    address=ui->txtAddress->text();
    adminid=ui->txthiddenid->text();

    if(db.saveRecord(id,firstName,lastName,email,tel,address,adminid))
    {
        QMessageBox::information(this,tr("Save"),tr("Contact Details successfully saved..!"));
    }
    else
    {
        QMessageBox::critical(this,tr("Error Message"),tr("Contact Details not saved successfully..!"));
    }

    //To refresh home form table
    QSqlQueryModel *model=new QSqlQueryModel();
    con.conOpen();
    QSqlQuery* quary2=new QSqlQuery(con.addressdb);
    quary2->prepare("SELECT firstName,lastName,email,tel,address FROM tblinfo WHERE adminid='"+ui->txthiddenid->text()+"'");
    quary2->exec();
    model->setQuery(*quary2);
    ui->tblInfo->setModel(model);

    con.conClose();
    qDebug()<<(model->rowCount());
}

void Home::on_btnUpdate_clicked()
{
    //Update information to database
    Database con;
    QString id,firstName,lastName,email,tel,address;

    id=ui->txtID->text();
    firstName=ui->txtFname->text();
    lastName=ui->txtLname->text();
    email=ui->txtEmail->text();
    tel=ui->txtTel->text();
    address=ui->txtAddress->text();

    if(db.updateRecord(id,firstName,lastName,email,tel,address))
    {
        QMessageBox::information(this,tr("Update"),tr("Contact Details successfully updated..!"));
    }
    else
    {
        QMessageBox::critical(this,tr("Error Message"),tr("Contact Details not updated successfully..!"));
    }

    //To refresh home form table
    QSqlQueryModel *model=new QSqlQueryModel();
    con.conOpen();
    QSqlQuery* quary2=new QSqlQuery(con.addressdb);
    quary2->prepare("SELECT firstName,lastName,email,tel,address FROM tblinfo WHERE adminid='"+ui->txthiddenid->text()+"'");
    quary2->exec();
    model->setQuery(*quary2);
    ui->tblInfo->setModel(model);

    con.conClose();
    qDebug()<<(model->rowCount());
}

void Home::on_btnDelete_clicked()
{
    //Delete information to database
    Database con;
    QString id;

    id=ui->txtID->text();


    if(db.deleteRecord(id))
    {
        QMessageBox::information(this,tr("Delete"),tr("Contact Details successfully deleted..!"));
    }
    else
    {
       QMessageBox::critical(this,tr("Error Message"),tr("Contact Details not deleted successfully..!"));
    }

    //To refresh home form table
    QSqlQueryModel *model=new QSqlQueryModel();
    con.conOpen();
    QSqlQuery* quary2=new QSqlQuery(con.addressdb);
    quary2->prepare("SELECT firstName,lastName,email,tel,address FROM tblinfo WHERE adminid='"+ui->txthiddenid->text()+"'");
    quary2->exec();
    model->setQuery(*quary2);
    ui->tblInfo->setModel(model);

    con.conClose();
    qDebug()<<(model->rowCount());
}

void Home::on_btnLoad_clicked()
{
}

void Home::on_btnClear_clicked()
{
    //Clear Fields
    ui->txtID->clear();
    ui->txtFname->clear();
    ui->txtLname->clear();
    ui->txtEmail->clear();
    ui->txtTel->clear();
    ui->txtAddress->clear();
}

void Home::on_tblInfo_clicked(const QModelIndex &index)
{
    //load information from table to text fields
    QString selectedValue=ui->tblInfo->model()->data(index).toString();

    Database con;
    if(!con.conOpen())
    {
        qWarning()<<"Databese Connection Failed..!";
        return;
    }

    con.conOpen();
    QSqlQuery quary;
    quary.prepare("SELECT * FROM tblinfo WHERE id='"+selectedValue+"' or firstName='"+selectedValue+"' or lastName='"+selectedValue+"' or email='"+selectedValue+"' or tel='"+selectedValue+"' or address='"+selectedValue+"'");

    if(quary.exec())
    {
        while(quary.next())
        {
            ui->txtID->setText(quary.value(0).toString());
            ui->txtFname->setText(quary.value(1).toString());
            ui->txtLname->setText(quary.value(2).toString());
            ui->txtEmail->setText(quary.value(3).toString());
            ui->txtTel->setText(quary.value(4).toString());
            ui->txtAddress->setText(quary.value(5).toString() );
        }
        con.conClose();
    }
    else
    {
        QMessageBox::critical(this,tr("Error Message"),quary.lastError().text());
    }
}

void Home::on_btnLogout_clicked()
{
    QMessageBox::StandardButton selectOption;
      selectOption = QMessageBox::question(this, "Are you sure?", "Quit?",
                                    QMessageBox::Yes|QMessageBox::No);
      if (selectOption == QMessageBox::Yes) {
          ui->txthiddenid->clear();
          this->close();
          //Login loginwindow;
          //
      } else {

      }

}

void Home::on_pushButton_clicked()
{
    if(ui->txtFname->text()!="")
    {
    writefile();
    qInfo()<<"Contact Details Succesfully Save To Text File...!";
    QMessageBox::information(this,tr("Save to text file"),tr("Contact Details Succesfully Save To Text File...!"));
    }
    else
    {
        qWarning()<<"Please select record from the contact table...!";
        QMessageBox::information(this,tr("Save to text file"),tr("Please select record from the contact table...!"));
    }
}

void Home::writefile()
{
    QString firstName = ui->txtFname->text();
    QString lastName = ui->txtLname->text();
    QString email = ui->txtEmail->text();
    QString tel = ui->txtTel->text();
    QString address = ui->txtAddress->text();

    QString filename = ui->txtFname->text() + "'s Contact Details.txt";
    QFile file(filename);
    file.open(QIODevice::WriteOnly|QIODevice::Text);
    QTextStream output(&file);
    output<<"First Name: "+firstName<<endl;
    output<<"Last Name: "+lastName<<endl;
    output<<"Email: "+email<<endl;
    output<<"Telephone NO: "+tel<<endl;
    output<<"Address: "+address<<endl;
    file.close();
}
