#-------------------------------------------------
#
# Project created by QtCreator 2015-11-10T22:33:37
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyAddressBook
TEMPLATE = app


SOURCES += main.cpp\
        login.cpp \
    home.cpp \
    register.cpp \
    user.cpp \
    database.cpp \
    record.cpp

HEADERS  += login.h \
    home.h \
    register.h \
    user.h \
    database.h \
    record.h

FORMS    += login.ui \
    home.ui \
    register.ui

DISTFILES += \
    ../../Desktop/Address-book-icon.png

RESOURCES += \
    resource.qrc

DESTDIR = $$PWD
