#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>
#include <QObject>
#include "user.h"
#include <QDebug>
#include <QFileDialog>

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = 0);
    QSqlDatabase addressdb;
    bool conOpen();
    void conClose();
    bool userAuthantication(QString username, QString password);
    User *getUser(QString name, QString password);
    bool saveRecord(QString id, QString firstName, QString lastName, QString email, QString tel, QString address, QString adminid);
    bool updateRecord(QString id, QString firstName, QString lastName, QString email, QString tel, QString address);
    bool deleteRecord(QString id);

signals:

public slots:
};

#endif // DATABASE_H
