#ifndef HOME_H
#define HOME_H

#include <QDialog>
#include "login.h"
#include "database.h"

namespace Ui {
class Home;
}

class Home : public QDialog
{
    Q_OBJECT

public:
    explicit Home(QWidget *parent = 0);
    Database db;
    ~Home();

private slots:
    void on_btnSave_clicked();

    void on_btnUpdate_clicked();

    void on_btnDelete_clicked();

    void on_btnLoad_clicked();

    void on_btnClear_clicked();

    void on_tblInfo_clicked(const QModelIndex &index);

    void on_btnLogout_clicked();

    void on_pushButton_clicked();

private:
    Ui::Home *ui;
    void writefile();
};

#endif // HOME_H
