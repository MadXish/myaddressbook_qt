#include "login.h"
#include "ui_login.h"
#include <QMessageBox>

Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);

    try
    {
        if(!db.conOpen())
        {
            ui->lblStatus->setText("Database Connection filed...!");
        }
        else
        {
           ui->lblStatus->setText("Welcome to My Address Book...!");
        }
    }
    catch(...)
    {
        qDebug()<<"Database Connection error...!";

    }

//    if(!db.conOpen())
//        ui->lblStatus->setText("Database Connection filed...!");
//    else
//        ui->lblStatus->setText("Database Connection successful...!");

    ui->loginid->setDisabled(true);
    ui->loginid->setVisible(false);

    //ui->lblStatus->setVisible(false);

}

QString Login::loginID;

Login::~Login()
{
    delete ui;
}

void Login::on_btnLogin_clicked()
{
    //Application Login
    QString username,password;

    username=ui->txtUserName->text();
    password=ui->txtPassword->text();

    if(username != "" && password != "")
    {
       if(db.userAuthantication(username,password))
       {
        loginID = db.getUser(username,password)->getAdminid();
        this->hide();
        Home homewindow;
        homewindow.setModal(true);
        homewindow.exec();
        this->show();
        }
       else
       {
           QMessageBox::information(this,tr("Login Error"),tr("Username and Password Wrong..!"));
       }
    }
    else
    {
        QMessageBox::information(this,tr("Login Error"),tr("Enter Username and Password..!"));
    }
}

void Login::on_btnSignup_clicked()
{
    this->hide();
    Register regwindow;
    regwindow.setModal(true);
    regwindow.exec();
    this->show();
}

void Login::on_actionClose_2_triggered()
{
    this->close();
}

void Login::on_actionClose_triggered()
{
    this->hide();
    Register regwindow;
    regwindow.setModal(true);
    regwindow.exec();
    this->show();
}

